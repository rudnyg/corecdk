#!/usr/bin/env python3
import os

from aws_cdk import core as cdk

# For consistency with TypeScript code, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.
from aws_cdk import core

from cirrus_stack_core.cirrus_cdk_stack import CirrusCoreStack
from custom_stack.custom_stack import CustomStack

app = core.App()
core_stack = CirrusCoreStack(app, "CirrusCoreStack")

# Example of getting a variable from another stack
catalog_bucket = core_stack.catalog_bucket

# Can add additional policies at any time.
# core_stack.cirrus_role.add_to_policy()


# Define custom stack
custom_stack = CustomStack(app, "LambdaStack", func_path="custom_stack/user_lambdas")
app.synth()
