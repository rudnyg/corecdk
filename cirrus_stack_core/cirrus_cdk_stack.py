from aws_cdk import (
    core as cdk,
    aws_s3 as s3,
    aws_sqs as sqs,
    aws_sns as sns,
    aws_dynamodb as dynamodb
)

from aws_cdk import core

from cirrus_stack_core.functions.job_step_function import JobPollerStack
from cirrus_stack_core.iam.roles import CirrusRole


class CirrusCoreStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        stage = kwargs.get("stage", "dev")

        # Catalog bucket
        self.catalog_bucket = s3.Bucket(
            self,
            id = f"CatalogBucket-{stage}",
            bucket_name = f"catalog-bucket-{stage}",
            versioned = True,
        )

        # Queue
        self.process_queue = sqs.Queue(
            self,
            id= "ProcessQueue",
            queue_name = f"ProcessQueue-{stage}",
        )

        # SNS
        self.sns_queue_topic = sns.Topic(
            self,
            id="QueueTopic",
            topic_name = f"CatalogBucket-{stage}"
        )

        # Dynamodb Table
        # create dynamo table
        self.state_table = dynamodb.Table(
            self,
            id= "demo_table",
            table_name = f"StateTable-{stage}" ,
            partition_key = dynamodb.Attribute(
                name = "id",
                type = dynamodb.AttributeType.STRING
            )
        )

        # A step function Setup
        self.step_function = JobPollerStack(self, id="JobStepFunction")

        # Main role
        role = CirrusRole(self, id = "CirrusRoleId")
        self.cirrus_role = role.main_role()

        # Outputs
        core.CfnOutput(
            self,
            id = "S3CatalogBucket",
            description = "S3 Catalog Bucket",
            value = self.catalog_bucket.bucket_name,
        )

        # This line ensures that all resources have this tag
        core.Tags.of(self).add("Project", "Cirrus Core")

        # Example hoe to make sure bucket is encrypted
        # core.Aspects.of(self).add(EncryptionAspect())


