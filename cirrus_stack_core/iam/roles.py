from aws_cdk import(
    core as cdk,
    aws_iam as iam
)


class CirrusRole:
    def __init__(self, scope: cdk.Construct, id: str):
        self.__scope = scope
        self.__id = id

    def main_role(self):
        main_role = iam.Role(
            scope=self.__scope,
            id=self.__id,
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            role_name="CirrusRole",
        )

        main_role.add_to_policy(
            iam.PolicyStatement(
                actions = [
                    "states:SendTaskSuccess",
                    "states:SendTaskFailure",
                    "states:SendTaskHeartbeat",
                ],
                resources = ["*"],
            )
        )
        main_role.add_to_policy(
            iam.PolicyStatement(
                actions = [
                    "states:SendTaskSuccess",
                    "states:SendTaskFailure",
                    "states:SendTaskHeartbeat",
                ],
                resources = ["*"],
            )
        )
        main_role.add_to_policy(
            iam.PolicyStatement(
                actions = [
                    "states:SendTaskSuccess",
                    "states:SendTaskFailure",
                    "states:SendTaskHeartbeat",
                ],
                resources = ["*"],
            )
        )

        return main_role