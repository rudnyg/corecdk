import jsii
from aws_cdk import (
    core as cdk,
    aws_s3 as s3
)


@jsii.implements(cdk.IAspect)
class EncryptionAspect:
    def visit(self, construct):
        # Filter for only buckets
        if isinstance(construct, s3.CfnBucket):
            # print('----')
            # print(type(construct))
            if str(construct.bucket_name).__contains__('prod'):
                if str(construct.bucket_encryption) == 'None':
                    construct.node.add_error("Destination must be encrypted.")


@jsii.implements(cdk.IAspect)
class CheckTerminationProtection:

    def visit(self, stack):
        if isinstance(stack, cdk.Stack):
            if not stack.termination_protection:
                stack.node.add_warning("This stack does not have termination protection.")

