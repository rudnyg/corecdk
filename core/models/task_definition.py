from enum import Enum
from typing import List, Dict, Optional, TypedDict
from pydantic import BaseModel


class Workflows(str, Enum):
    publish_only = 'publish-only'
    mirror = 'mirror'
    create_cog = 'create_cog'


# `total=False` means keys are non-required
class OutputOptions(TypedDict, total=False):
    path_template: str
    surname: str
    collections: dict


class BaseTask(BaseModel):
    workflow: Workflows
    output_options = OutputOptions
    tasks: Optional[Dict] = None