"""Misc utils for application."""
from string import Formatter


class MissingFormatter(Formatter):
    """Class for formatting missing keys."""

    def get_value(self, key, args, kwds):
        """Get values or name of key if not found."""
        if isinstance(key, str):
            try:
                return kwds[key]
            except KeyError:
                return key
        else:
            return Formatter.get_value(key, args, kwds)


def clean_json_str(value: str) -> str:
    """Strips special characters to remove errors on json.loads."""
    import re

    p = re.compile("(?<!\\\\)'")
    return p.sub('"', value)
