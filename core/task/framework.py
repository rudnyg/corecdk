"""Mini lambda framework."""
from functools import wraps
import logging
import typing

from core.task import task_models as tm
from core.task.task_handler import lambda_handler

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


def core_task(
    request_model: tm.CoreBaseModel = None, response_model: tm.CoreBaseModel = None
):
    """Core framework decorator used on aws lambda handlers.

    Args:
        request_model (CoreBaseModel): Request model class.
        response_model (CoreBaseModel): Response model class.

    Returns:
        CoreModelWrapper
    """

    def model_type_validation(
        req_model: tm.CoreBaseModel, res_model: tm.CoreBaseModel
    ) -> tm.CoreModelWrapper:
        model_req = tm.CoreModel(valid=True, model=req_model, errors=[])
        model_res = tm.CoreModel(valid=True, model=res_model, errors=[])

        if req_model:
            if not issubclass(req_model, tm.CoreBaseModel):
                model_req = tm.CoreModel(
                    valid=False, model=None, errors=[tm.INVALID_CORE_BASE]
                )

        if res_model:
            if not issubclass(res_model, tm.CoreBaseModel):
                model_res = tm.CoreModel(
                    valid=False, model=None, errors=[tm.INVALID_CORE_BASE]
                )

        return tm.CoreModelWrapper(request_model=model_req, response_model=model_res)

    def decorator(handler_func):
        @wraps(handler_func)
        def wrapper(event: dict, context: typing.Any):
            model_validation = None

            # Decorate the handler with your lambda class.
            decorated_handler = lambda_handler(handler_func)

            # Handle if models provided.
            if request_model or response_model:
                model_validation = model_type_validation(
                    req_model=request_model, res_model=response_model
                )

            # Return the decorated function.
            return decorated_handler(event, context, model_validation)

        return wrapper

    return decorator
