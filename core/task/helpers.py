"""Aws Lambda Event objects."""
import abc
from enum import Enum
import json
import logging
import typing

from core.models.utils import clean_json_str

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class StatusCode(Enum):
    """Http status code enums class."""

    NO_RETURN = 99
    SUCCESS = 200
    SUCCESS_CREATED = 201
    VALIDATION_ERROR = 400
    NOT_FOUND = 404
    ACCESS_ERROR = 405
    SYSTEM_ERROR = 500


class ResponseSuccess:
    """Class for formatting response status that are successful."""

    @staticmethod
    def format(status_code, response_body):
        """Formats response for lambda."""
        return {
            "statusCode": status_code,
            "body": json.dumps(response_body, indent=4, sort_keys=True),
            "headers": {"Content-Type": "application/json"},
        }


class ResponseFailure:
    """Class for formatting response status that are unsuccessful."""

    RESOURCE_ERROR = "NotFoundError"
    PARAMETERS_ERROR = "ValidationError"
    SYSTEM_ERROR = "InternalServerError"
    ACCESS_ERROR = "AccessDeniedError"

    STATUS_CODES = {
        StatusCode.VALIDATION_ERROR.value: PARAMETERS_ERROR,
        StatusCode.NOT_FOUND.value: RESOURCE_ERROR,
        StatusCode.ACCESS_ERROR.value: ACCESS_ERROR,
        StatusCode.SYSTEM_ERROR.value: SYSTEM_ERROR,
    }

    @classmethod
    def format(cls, response_body, error_status):
        """Formats response for lambda."""
        return {
            "statusCode": error_status,
            "body": json.dumps(
                {
                    "error": {
                        "code": ResponseFailure.STATUS_CODES[error_status],
                        "message": response_body,
                    },
                    "data": None,
                },
                indent=4,
                sort_keys=True,
            ),
            "headers": {"Content-Type": "application/json"},
        }


class LambdaEventType(Enum):
    """Lambda Event Enums."""

    API_GATEWAY = "API_GATEWAY"
    DICT = "BASE"
    IoT = "IoT"
    S3 = "S3"
    SQS = "SQS"
    SNS = "SNS"


class LambdaEvent(abc.ABC):
    """Base lambda event type."""

    def __init__(self, event: dict) -> None:
        """Initialize the Lambda Event Class."""
        if not isinstance(event, dict):
            raise TypeError("Event must be a dictionary")

        self._event = event
        LOG.info(f"Parsing event: {self.event_type}")

    @property
    @abc.abstractmethod
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""

    @property
    @abc.abstractmethod
    def body(self) -> typing.Optional[typing.Dict[str, typing.Any]]:
        """Event body/payload."""

    @property
    def full_event(self) -> typing.Dict[str, typing.Any]:
        """Full event with meta data."""
        return self._event

    def __repr__(self) -> str:
        """Event representation."""
        return str(self._event)


class LambdaEventParser:
    """AWS lambda event type parser."""

    @classmethod
    def parse(cls, event: dict) -> LambdaEvent:
        """Initialize the event class."""
        if not event:
            raise ValueError("Event body cannot be null.")

        if "httpMethod" in event:
            return HttpEvent(event=event)

        if "Records" in event:
            record_one = event["Records"][0]
            if record_one.get("eventSource") == "aws:sqs":
                return SqsEvent(event=event)
            if record_one.get("eventSource") == "aws:s3":
                return S3Event(event=event)
            if record_one.get("EventSource") == "aws:sns":
                return SnsEvent(event=event)

        return BasicEvent(event=event)


class SnsEvent(LambdaEvent):
    """AWS SNS event."""

    def __init__(self, event: dict) -> None:
        """Initialize the SNS class."""
        super().__init__(event)
        self.__event = event

        if "Records" not in self.__event:
            raise ValueError("Event must be a sns event.")

        if self.__event["Records"][0].get("EventSource") != "aws:sns":
            raise ValueError("Event must be a sns event.")

        LOG.info(f"Parsing event: {self.event_type}")

    @property
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""
        return LambdaEventType.SNS

    @property
    def body(self) -> typing.Optional[typing.Dict[str, typing.Any]]:
        """Event body/payload."""
        content = self.__event["Records"][0]["Sns"]["Message"]

        if isinstance(content, dict):
            return content

        try:
            content = clean_json_str(content)
            return json.loads(content)
        except (ValueError, TypeError) as ve:
            LOG.info(f"Unable to parse sns event: {ve}")
            # Content is just string or value.
            if content:
                return content
            return None

    @property
    def message_id(self) -> typing.Dict[str, typing.Any]:
        """Id iof message."""
        return super().full_event["Records"][0]["Sns"]["MessageId"]

    @property
    def attributes(self) -> typing.Dict[str, typing.Any]:
        """Event Meta data/attributes."""
        return super().full_event["Records"][0]["Sns"]["MessageAttributes"]


class SqsEvent(LambdaEvent):
    """AWS SQS event."""

    def __init__(self, event: dict) -> None:
        """Initialize the SQS class."""
        super().__init__(event)
        self.__event = event

        if "Records" not in self.__event:
            raise ValueError("Event must be a sqs event.")

        LOG.info(f"Parsing event: {self.event_type}")

    @property
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""
        return LambdaEventType.SQS

    @property
    def body(self) -> typing.Dict[str, typing.Any]:
        """Event body/payload."""
        content = self.__event["Records"][0]["body"]
        try:
            return json.loads(content)
        except (ValueError, TypeError):
            # Content is just string or value.
            if content:
                return content
            return {}

    @property
    def attributes(self) -> typing.Dict[str, typing.Any]:
        """Event Meta data/attributes."""
        return self.__event["Records"][0]["Attributes"]


class HttpEvent(LambdaEvent):
    """Api Gateway Lambda Event."""

    def __init__(self, event: dict) -> None:
        """Initialize the API Gateway class."""
        super().__init__(event)
        self.__event = event

        if "httpMethod" not in self.__event:
            raise ValueError("Event must be a Api Gateway event.")

        LOG.info(f"Parsing event: {self.event_type}")

    @property
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""
        return LambdaEventType.API_GATEWAY

    @property
    def body(self) -> typing.Dict[str, typing.Any]:
        """Event body/payload."""
        try:
            return json.loads(self.__event["body"])
        except (ValueError, TypeError) as ve:
            LOG.info(f"Unable to parse http event: {ve}")
            return {}

    @property
    def path(self) -> str:
        """The http path."""
        return self.__event.get("path", "")

    @property
    def path_params(self) -> typing.Dict[str, typing.Any]:
        """Params as key/value pair."""
        return self.__event.get("pathParameters", {})

    @property
    def method(self) -> str:
        """Http method of call."""
        return self.__event.get("httpMethod", "")

    @property
    def query_params(self) -> typing.Dict[str, typing.Any]:
        """Query params as key/value pair."""
        return self.__event.get("queryStringParameters", {})

    @property
    def query_params_list(self) -> typing.Dict[str, typing.List]:
        """Query params as key/value pair."""
        return self.__event.get("multiValueQueryStringParameters", {})


class BasicEvent(LambdaEvent):
    """Basic Event, can be a custom integration request."""

    def __init__(self, event: dict) -> None:
        """Initialize the Basic Event class."""
        super().__init__(event)
        self.__event = event

        LOG.info(f"Parsing event: {self.event_type}")

    @property
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""
        return LambdaEventType.DICT

    @property
    def body(self) -> typing.Dict[str, typing.Any]:
        """Event body/payload."""
        return self.__event


class S3Event(LambdaEvent):
    """S3 Event, can be a custom integration request."""

    def __init__(self, event: dict) -> None:
        """Initialize the S3 Event class."""
        super().__init__(event)
        self.__event = event["Records"][0]

    @property
    def event_type(self) -> LambdaEventType:
        """The event type enum, see `LambdaEventType`."""
        return LambdaEventType.S3

    @property
    def body(self) -> typing.Dict[str, typing.Any]:
        """Event body/payload."""
        return self.__event

    @property
    def bucket_name(self) -> str:
        """The name of bucket that triggered the event."""
        return self.__event["s3"]["bucket"]["name"]

    @property
    def file_name(self) -> str:
        """The name of object that triggered the event."""
        return self.__event["s3"]["object"]["key"]

    @property
    def file_extension(self) -> str:
        """The name of object that triggered the event."""
        from pathlib import Path

        extension = Path(self.__event["s3"]["object"]["key"]).suffix
        return extension
