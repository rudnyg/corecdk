"""Decorator for Aws lambda functions."""
from functools import update_wrapper
import logging
import traceback
import typing
import uuid

from core.task import task_models as cm
import core.task.helpers as res
from cirruslib import Catalog, get_task_logger


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class LambdaDecorator(object):
    """Class for simplifying the creation of decorators for use on Lambda handlers."""

    def __init__(self, handler: typing.Callable) -> None:
        """Initializes the lambda handler class."""
        update_wrapper(self, handler)
        self.handler = handler

    def __call__(self, event: dict, context: dict, model: cm.CoreModelWrapper = None):
        """Wrapper function to ensure before and after functions called."""
        try:
            return self.after(self.handler(*self.before(event, context, model)))
        except Exception as exception:
            return self.on_exception(exception)

    def before(self, event: dict, context: dict, model: cm.CoreModelWrapper = None):
        """Called before entering function."""
        return event, context, model

    def after(self, return_value: typing.Any) -> typing.Any:
        """Called after exiting function."""
        return return_value

    def on_exception(self, exception: Exception) -> None:
        """Called if exception is raised."""
        raise exception


def before(func):
    """Runs before entering function handler."""

    class BeforeDecorator(LambdaDecorator):
        def before(
            self, event: dict, context: dict, model: cm.CoreModelWrapper = None
        ) -> typing.Callable:
            return func(event, context, model)

    return BeforeDecorator


def after(func):
    """Runs after entering function handler."""

    class AfterDecorator(LambdaDecorator):
        def after(self, return_value: typing.Any) -> typing.Callable:
            return func(return_value)

    return AfterDecorator


def on_exception(func: typing.Callable):
    """Catches exception raised from lambda handler.

    Args:
        func (): Function with decorator.

    Returns: 'OnExceptionDecorator'
    """

    class OnExceptionDecorator(LambdaDecorator):
        """Exception decorator class."""

        def on_exception(self, exception: Exception):
            """Catches exception raised by handler function.

            Args:
                exception (): Exception object.

            Returns: Function
            """
            return func(exception)

    return OnExceptionDecorator


class lambda_handler(LambdaDecorator):  # pylint: disable=invalid-name
    """Lambda handler class used for request and response formatting."""

    def before(
        self, event: dict, context: typing.Any, models: cm.CoreModelWrapper = None
    ) -> typing.Tuple[cm.CoreRequestEvent, typing.Any]:
        """Used to do automatic logging of request and formatting input event.

        Args:
            event (dict): The incoming event.
            context (any): Unused field from lambda framework.
            models (CoreModelWrapper): Request and Response model definitions.

        Returns: tuple(CoreRequestEvent, Any)
        """
        LOG.info(f"[REQUEST]- Event: {event}")
        # Parse lambda event.
        lambda_event = res.LambdaEventParser.parse(event=event)

        # if no model provided at all, create core event with no models.
        if not models:
            core_event = cm.CoreRequestEvent(event=lambda_event, model=None)
            return core_event, context

        # If the model type validation fails return failed validation
        # Do not load model from event.
        if not models.request_model.valid:
            core_event = cm.CoreRequestEvent(
                event=lambda_event, model=models.request_model
            )
            return core_event, context

        # Load model from event body
        model = cm.CoreBaseModel.load_event(
            event=lambda_event, model=models.request_model.model
        )

        core_event = cm.CoreRequestEvent(event=lambda_event, model=model)
        return core_event, context

    def after(self, func_response) -> typing.Dict[str, typing.Any]:
        """Actions performed on handler after calling the lambda function.

        Args:
            func_response (func): Decorated function.

        Returns:
            `ResponseSuccess` if successful, `ResponseFailure` otherwise.
        """
        LOG.info(f"[RESPONSE]: {func_response}")
        status_code, message = func_response

        if status_code == res.StatusCode.NO_RETURN:
            return message

        if isinstance(status_code, res.StatusCode):
            status_code = status_code.value

        if status_code < 400:
            return res.ResponseSuccess.format(
                status_code=status_code, response_body=message
            )

        return res.ResponseFailure.format(
            error_status=status_code, response_body=message
        )

    def on_exception(self, exception: Exception):
        """Catch exceptions for unhandled exceptions.

        Args:
            exception (class): Exception class.

        Returns:
            `ResponseFailure`
        """
        error_id = str(uuid.uuid4())[:8]
        formatted_traceback = traceback.format_exc()
        LOG.error(f"[{error_id}]: {formatted_traceback}")

        return res.ResponseFailure.format(
            response_body=f"Internal Server Error. Reference id: {error_id}",
            error_status=500,
        )
