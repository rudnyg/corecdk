"""Core framework models."""
from dataclasses import dataclass
import json
import logging
import typing

from mypy_extensions import TypedDict
from pydantic.main import BaseModel, ValidationError

from core.task.helpers import LambdaEvent

INVALID_CORE_BASE = "Request model most inherit from `CoreBaseModel`"


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


ResponseStatus = typing.Tuple[bool, typing.Union[str, typing.Dict[str, typing.Any]]]


class ResponseDict(TypedDict):
    """Response Dictionary object."""

    message_id: typing.Optional[str]
    error_code: typing.Optional[str]
    message: typing.Optional[str]


# ResponseStatus = typing.Tuple[bool, ResponseDict]


@dataclass
class CoreModel:
    """Model Data type for lambda framework.

    Attributes:
        valid (bool): True iof model is valid, false otherwise.
        model (object): Holds the current model object.
        errors (list): List of validation errors.
    """

    valid: bool
    model: typing.Any
    errors: typing.Union[typing.List[str], typing.Dict[str, typing.Any]]


class CoreBaseModel(BaseModel):
    """Model base class. Models need to inherit."""

    def __init__(self, **data: typing.Any) -> None:
        """Initializes the base model class."""
        try:
            super().__init__(**data)
        except ValidationError as e:
            raise CoreValidationError(e)

    @classmethod
    def load_event(cls, model: typing.Any, event: LambdaEvent) -> CoreModel:
        """Loads model object from json.

        Args:
            model (object): Model you want to try and load.
            event (LambdaEvent): Incoming lambda event.

        Returns:
            `CoreModel` of type input `model`
        """
        LOG.info(f"Event: {event}")

        event_body: dict = event.body
        try:
            LOG.info(
                f"Model: {model}, Event_Type: {type(event_body)}, Event: {event_body}"
            )

            loaded_model = model(**event_body)
            return CoreModel(valid=True, model=loaded_model, errors=[])
        except ValidationError as e:
            LOG.info("Error Validating model.")
            return CoreModel(valid=False, model=event_body, errors=e.json())
        except CoreValidationError as e:
            LOG.info("Error Validating model.")
            return CoreModel(valid=False, model=event_body, errors=e.errors)


class CoreValidationError(BaseException):
    """Core validation wrapper class."""

    def __init__(self, exception: ValidationError) -> None:
        """Initializes the base exception class."""
        error_json = json.loads(exception.json())
        self.errors = {dt["loc"][0]: dt["msg"] for dt in error_json}


@dataclass
class CoreRequestEvent:
    """Data class for request event."""

    event: LambdaEvent
    model: typing.Union[CoreModel, dict]  # if not valid, model will be dictionary


@dataclass
class CoreModelWrapper:
    """Data class to wrap response and request models."""

    request_model: CoreModel
    response_model: CoreModel
