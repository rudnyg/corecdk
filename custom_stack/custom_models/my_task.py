from core.models.task_definition import BaseTask


class CustomTask(BaseTask):
    task_values: dict
    url: str
