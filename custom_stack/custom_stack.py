import os
from aws_cdk import (
    core as cdk,
    aws_lambda as lambda_func
)


class CustomStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, func_path: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        if func_path:
            for root, dirs, files in os.walk(func_path, topdown = False):

                # Done traversing so break
                if root == func_path:
                    break

                workflow = root.replace(f"{func_path}/", '')

                # Only add if task exist
                if 'task.py' in files:

                    # print(f"Workflow: {workflow}")

                    lambda_func.Function(
                        self,
                        id=f"{workflow}_lambda_function",
                        runtime = lambda_func.Runtime.PYTHON_3_6,
                        handler = "task.handler",
                        code = lambda_func.Code.asset(root)
                    )
                elif 'Dockerfile' in files:
                    ## Create new Container Image.
                    directory = os.path.join(os.getcwd(), root)
                    print(f"Dir: {directory}")
                    ecr_image = lambda_func.EcrImageCode.from_asset_image(
                        directory = directory
                    )

                    lambda_func.Function(
                        self,
                        id = f"{workflow}_lambda_function",
                        description = "Sample Lambda Container Function",
                        code = ecr_image,
                        ##
                        ## Handler and Runtime must be *FROM_IMAGE*
                        ## when provisioning Lambda from Container.
                        ##
                        handler = lambda_func.Handler.FROM_IMAGE,
                        runtime = lambda_func.Runtime.FROM_IMAGE,
                        environment = {"hello": "world"},
                        function_name = f"{workflow}_lambda_function",
                        memory_size = 128,
                        reserved_concurrent_executions = 10,
                        timeout = cdk.Duration.seconds(60),
                    )





