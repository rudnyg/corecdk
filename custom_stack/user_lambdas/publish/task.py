from core.models.task_definition import BaseTask
from core.task.framework import core_task
from core.task.task_models import CoreRequestEvent


@core_task(request_model = BaseTask, response_model =None)
def handler(event: CoreRequestEvent):
    """A sample core task

    Notifications are sent asynchronously. This lambda function
    handles message callbacks based on the service that sent the
    message will determine if the delivery was successful or not.
    Args:
        request (CoreRequestEvent): Incoming request.
            CoreRequestEvent.model = Request model if valid, Dictionary otherwise.
            CoreRequestEvent.event = Request event
        _ (None): Unused field from lambda.

    Returns: dict
    """
